ARG BASE_RUBY_IMAGE_TAG='3.1.3-alpine'

FROM ruby:${BASE_RUBY_IMAGE_TAG} as base

ARG RAILS_ENV

ARG TARGETARCH

RUN if [ "${TARGETARCH}" = "arm64" ]; then \
  apk add --no-cache \
  gcompat; \
  fi;

RUN apk add --no-cache \
  build-base \
  curl \
  git \
  libcurl \
  postgresql-dev \
  postgresql-client \
  tzdata \
  shared-mime-info

WORKDIR /app

COPY Gemfile Gemfile.lock ./

RUN gem install bundler -v 2.3.15

RUN MAKE="make --jobs 5" bundle install --jobs=5 --no-cache --retry=5 && \
    bundle config unset rubygems.pkg.github.com

COPY . ./

ENTRYPOINT ["/app/bin/entrypoint"]

CMD ["bundle", "exec", "rails", "server", "-p", "3000"]
