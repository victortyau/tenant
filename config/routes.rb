Rails.application.routes.draw do
  resources :users, except: [:destroy] do
    resources :posts, except: [:destroy]
  end
end
