class User < ApplicationRecord
  has_many :posts
  validates :username, presence: true, uniqueness: true
  validates :code, presence: true, uniqueness: { scope: :id }
end
