class PostsController < ApplicationController
  def index
    display(user.posts)
  end

  def show
    display(post)
  end

  def create
    @post = user.posts.new(post_params)
    @post.save ? display(@post) : display(@post.errors)
  end

  def update
    post.update(post_params) ? display(post) : display(post.errors)
  end

  private

  def display(post)
    render json: post
  end


  def user
    @user ||= User.find_by(code: params[:user_id])
  end

  def post
    @post ||= Post.find(params[:id])
  end

  def post_params
    params.require(:post).permit(:title, :body)
  end
end
