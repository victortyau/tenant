class UsersController < ApplicationController

  def index
    display(users)
  end

  def show
    display(user)
  end

  def create
    @user = User.new(users_params)
    @user.save ? display(@user) : display(@user.errors)
  end

  def update
    user.update(users_params) ? display(user) : display(user.errors)
  end

  private

  def display(value)
    render json: value
  end

  def users
    @users ||= User.all
  end

  def user
    @user ||= User.find(params[:id])
  end

  def users_params
    params.require(:user).permit(:username, :code)
  end
end
